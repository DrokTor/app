<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dev_declaration".
 *
 * @property integer $id_declaration
 * @property integer $id_deviation
 * @property string $what
 * @property string $how
 * @property integer $locality
 * @property string $impact
 * @property string $immediate_actions
 * @property integer $process_step
 * @property integer $equipment_involved
 * @property integer $product_code
 * @property integer $batch_number
 */
class DevDeclaration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dev_declaration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_deviation', 'what', 'how', 'locality', 'impact', 'immediate_actions', 'process_step', 'equipment_involved', 'product_code', 'batch_number'], 'required'],
            [['id_deviation', 'locality', 'process_step', 'equipment_involved', 'product_code', 'batch_number'], 'integer'],
            [['what', 'how', 'impact', 'immediate_actions'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_declaration' => 'Id Declaration',
            'id_deviation' => 'Id Deviation',
            'what' => 'What',
            'how' => 'How',
            'locality' => 'Locality',
            'impact' => 'Impact',
            'immediate_actions' => 'Immediate Actions',
            'process_step' => 'Process Step',
            'equipment_involved' => 'Equipment Involved',
            'product_code' => 'Product Code',
            'batch_number' => 'Batch Number',
        ];
    }
}
