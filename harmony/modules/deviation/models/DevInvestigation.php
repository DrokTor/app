<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dev_investigation".
 *
 * @property integer $id_investigation
 * @property integer $id_deviation
 * @property integer $id_method
 * @property integer $id_ishikawa
 * @property string $origin_anomaly
 * @property integer $is_history
 * @property string $history_anomaly
 * @property string $batch_review
 * @property string $equipment_review
 * @property string $conformity
 * @property string $review_actions
 * @property string $conclusion
 * @property integer $deviation_lvl
 */
class DevInvestigation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dev_investigation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_deviation', 'id_method', 'id_ishikawa', 'origin_anomaly', 'is_history', 'history_anomaly', 'batch_review', 'equipment_review', 'conformity', 'review_actions', 'conclusion', 'deviation_lvl'], 'required'],
            [['id_deviation', 'id_method', 'id_ishikawa', 'is_history', 'deviation_lvl'], 'integer'],
            [['origin_anomaly', 'history_anomaly', 'batch_review', 'equipment_review', 'conformity', 'review_actions', 'conclusion'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_investigation' => 'Id Investigation',
            'id_deviation' => 'Id Deviation',
            'id_method' => 'Id Method',
            'id_ishikawa' => 'Id Ishikawa',
            'origin_anomaly' => 'Origin Anomaly',
            'is_history' => 'Is History',
            'history_anomaly' => 'History Anomaly',
            'batch_review' => 'Batch Review',
            'equipment_review' => 'Equipment Review',
            'conformity' => 'Conformity',
            'review_actions' => 'Review Actions',
            'conclusion' => 'Conclusion',
            'deviation_lvl' => 'Deviation Lvl',
        ];
    }
}
