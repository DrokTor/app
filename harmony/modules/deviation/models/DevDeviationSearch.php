<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DevDeviation;

/**
 * DevDeviationSearch represents the model behind the search form about `app\models\DevDeviation`.
 */
class DevDeviationSearch extends DevDeviation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_deviation', 'state', 'creator'], 'integer'],
            [['creation_date', 'creation_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DevDeviation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_deviation' => $this->id_deviation,
            'state' => $this->state,
            'creator' => $this->creator,
        ]);

        $query->andFilterWhere(['like', 'creation_date', $this->creation_date])
            ->andFilterWhere(['like', 'creation_time', $this->creation_time]);

        return $dataProvider;
    }
}
