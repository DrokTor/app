<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginContent('@app/views/layouts/main.php') ?>
    <div class="container-fluid ">
      <div class="row">
        <div class="col-md-12">
        <?php
            NavBar::begin([
                'brandLabel' => 'Deviation',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse' //navbar-fixed-top',
                ],
            ]);

            NavBar::end();
        ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
        <?php

          $deviation_menu= Html::tag("a","New deviation",["class"=>"pull-left","href"=>"./new"]).'<br>'.
         Html::tag("a","List deviations",["class"=>"pull-left","href"=>"./list"]).'<br>'.
         Html::tag("a","Data analysis",["class"=>"pull-left","href"=>"./data"]);
        echo Html::tag("div",$deviation_menu,["class"=>"pull-left"]); ?>
         </div>
        <div class="col-md-9">
        <?= $content ?>
        </div>
      </div>

    </div>



<?php $this->endContent() ?>

