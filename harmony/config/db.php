<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=harmony',
    'username' => 'root',
    'password' => 'pass',
    'charset' => 'utf8',
];
